#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "MyGL.h"

#include <ft2build.h>
#include FT_FREETYPE_H

FT_Library library;

static const GLuint WIDTH = 800;
static const GLuint HEIGHT = 480;
static const GLchar* vertex_shader_source =
    "#version 100\n"
    "attribute vec3 position;\n"
    "void main() {\n"
    "   gl_Position = vec4(position, 1.0);\n"
    "}\n";
static const GLchar* fragment_shader_source =
    "#version 100\n"
    "void main() {\n"
    "   gl_FragColor = vec4(1.0, 0.8, 0.0, 1.0);\n"
    "}\n";
static const GLfloat vertices[] = {
        -0.5f,  0.0f, 0.0f,
         0.5f,  0.0f, 0.0f,
        -0.5f, -0.5f, 0.0f,
         0.5f, -0.5f, 0.0f
};

GLint common_get_shader_program(const char *vertex_shader_source, const char *fragment_shader_source) {
    enum Consts {INFOLOG_LEN = 512};
    GLchar infoLog[INFOLOG_LEN];
    GLint fragment_shader;
    GLint shader_program;
    GLint success;
    GLint vertex_shader;

    /* Vertex shader */
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertex_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Fragment shader */
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(fragment_shader, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    }

    /* Link shaders */
    shader_program = glCreateProgram();
    glAttachShader(shader_program, vertex_shader);
    glAttachShader(shader_program, fragment_shader);
    glLinkProgram(shader_program);
    glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shader_program, INFOLOG_LEN, NULL, infoLog);
        printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
    return shader_program;
}

void error_callback(GLint error, const char* description)
{
    fprintf(stderr, "ERROR CALLBACK: %s\n", description);
}

void centerWindow(GLFWwindow *window, GLFWmonitor *monitor)
{
    if (!monitor) return;

    const GLFWvidmode* mode = glfwGetVideoMode(monitor);
    if (!mode) return;

    int monitorX, monitorY;
    glfwGetMonitorPos(monitor, &monitorX, &monitorY);

    int W, H;
    glfwGetWindowSize(window, &W, &H);

    glfwSetWindowPos(window,
            monitorX + (mode->width - W) / 2,
            monitorY + (mode->height - H) /2);
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    printf("keypress: %d\t%d\t%d\t%d\n", key, scancode, action, mods);
}

int run() {
    GLuint shader_program, vbo;
    GLint pos;
    
    FT_Error error = FT_Init_FreeType(&library);
    if (error) {
        std::fprintf(stderr, "freetype init error %d\n", error);
    }
    FT_Face face; const char* ft_path = "/usr/share/fonts/truetype/fonts-bpg-georgian/BPG_Courier_S_GPL&GNU.ttf";
    error = FT_New_Face(library, ft_path, 0, &face);
    if (error == FT_Err_Unknown_File_Format) {
        fprintf(stderr, "error %d: unknown format: %s\n", error, ft_path);
    } else if (error) {
        std::fprintf(stderr, "error %d\n", error);
    }
    if (error) return EXIT_FAILURE;
    else {
        std::printf("style_name: %s\n", face->style_name);
        std::printf("family_name: %s\n", face->family_name);
        std::printf("num_glyphs: %d\n", (int) face->num_glyphs);
        std::printf("face_flags: %d\n", (int) face->face_flags);
        std::printf("units_per_EM: %d\n", face->units_per_EM);
        std::printf("num_fixed_sizes: %d\n", face->num_fixed_sizes);
    }
    error = FT_Set_Char_Size(face, 0, 12 * 64, 72, 72);
    if (error) {
        fprintf(stderr, "error %d while setting char size\n", error);
        return error;
    }

    if (!glfwInit()) {
        // Initialization failed
    }
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_DOUBLEBUFFER, GL_TRUE);
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, __FILE__, nullptr/*glfwGetPrimaryMonitor()*/, nullptr);
    if (!window) {
        // Window or OpenGL context creation failed
    }
    glfwMakeContextCurrent(window);
    centerWindow(window, glfwGetPrimaryMonitor());
    glfwSetKeyCallback(window, key_callback);

    printf("GL_VERSION  : %s\n", glGetString(GL_VERSION) );
    printf("GL_RENDERER : %s\n", glGetString(GL_RENDERER) );

    shader_program = common_get_shader_program(vertex_shader_source, fragment_shader_source);
    pos = glGetAttribLocation(shader_program, "position");

    glClearColor(0.5f, 0.5f, 0.5f, 0.001f);
    glViewport(0, 0, WIDTH, HEIGHT);

    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(pos, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
    glEnableVertexAttribArray(pos);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    double time = glfwGetTime();
    unsigned int tick = 0;
    glfwWaitEventsTimeout(0.0001);
    while (!glfwWindowShouldClose(window)) {
        glfwWaitEvents();
        glClear(GL_COLOR_BUFFER_BIT);
        glUseProgram(shader_program);
        glFrontFace(GL_CW);
        glEnable(GL_CULL_FACE);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glfwSwapBuffers(window);

        ++tick;
        if (glfwGetTime() - time > 1) {
            time = glfwGetTime();
            printf("%f\t\t%u ticks\n", time, tick);

            tick = 0;
        }
        
    }
    glDeleteBuffers(1, &vbo);
    glfwDestroyWindow(window);
    glfwTerminate();
    return EXIT_SUCCESS;
}
