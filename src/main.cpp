#include "BadgeEngine.h"
#include "BadgePrograms.h"

#include <iostream>

int main(int argc, char* argv[])
{
    std::cout << "starting..." << std::endl;
    GEngine.Init();
    std::cout << "BadgeEngine initialized" << std::endl;
    MainMenuProgram inst0;
    GEngine.SetBadgeProgram(&inst0);
    GEngine.MainLoop();
    GEngine.Shutdown();

    return 0;
}
