#include "Platform.h"

#include "Constants.h"
#include "MyGL.h"

#include <GLFW/glfw3.h>

#include <chrono>
#include <iostream>
#include <thread>

#include <unistd.h>

Platform GPlatform;

void PlatformBase::Init() {}
void PlatformBase::DeInit() {}


#ifdef BCM_HOST
#else
#endif


void LinuxPlatform::Init()
{
    Platform::Init();
}

float LinuxPlatform::GetTimeSeconds() const
{
    return (float) glfwGetTime();
}

bool PollEvent()
{
    // glfw poll events
    glfwPollEvents();

    return false;
}
