SRC_DIR = ./src
OBJ_DIR = ./obj
BIN_DIR = ./bin
INC_DIR = ./inc

SRC_LIST = $(wildcard $(SRC_DIR)/*.cpp)
OBJ_LIST = $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRC_LIST))

CFLAGS = -g -pthread -Wall -I$(INC_DIR) -I/usr/include/freetype2 -I/usr/include/libpng16
LDFLAGS = -Wl,-z,now
LDLIBS = -lGLESv2 -lglfw -lfreetype
PROG_NAME = chartreuse

.PHONY: all clean $(PROG_NAME)

all: $(PROG_NAME)

$(PROG_NAME): $(OBJ_LIST)
	[ -d $(BIN_DIR) ] || mkdir $(BIN_DIR)
	$(CXX) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -o $(BIN_DIR)/$@ $^

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	[ -d $(OBJ_DIR) ] || mkdir $(OBJ_DIR)
	$(CXX) -c $(CFLAGS) -o $@ $<

clean:
	rm -rvf $(BIN_DIR) $(OBJ_DIR)
