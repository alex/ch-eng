#pragma once

#include "MyGL.h"
#include "BadgePrograms.h"

// BadgeEngine handles input and rendering loops
class BadgeEngine
{
public:
    BadgeEngine() = default;
    ~BadgeEngine() = default;

    void Init();
    void Shutdown();
    void SetBadgeProgram(BadgeProgram* prog);
    
    void MainLoop();
    void HandleTap(float x, float y);

    int GetWidth() const;
    int GetHeight() const;
    const char* GetPath() const;
};

extern BadgeEngine GEngine;
