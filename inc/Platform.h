#pragma once

#include "Constants.h"
#include "Vectors.h"

#include <string>

class PlatformBase
{
public:
    void Init();
    void DeInit();
    IVector2D GetWindowSize() const { return WindowSize; }
    IVector2D GetAppSize() const { return AppSize; }
    virtual float GetTimeSeconds() const = 0;
    virtual void Delay(unsigned int millis) const = 0;
    virtual bool PollEvent() = 0;
    void ScanInput() {}
    void SwapBuffers();
    bool PlatformRequestingExit() const { return bRequestExit; }
    const char* GetAppPath() const { return appPath.c_str(); }

private:
    std::string appPath;

protected:
    PlatformBase(const std::string& path)
        : appPath(path)
        , WindowSize(IVector2D(SIZE_X, SIZE_Y))
        , AppSize(IVector2D(APP_SIZE_X, APP_SIZE_Y))
    {}
    
    IVector2D WindowSize;
    IVector2D AppSize;
    bool bRequestExit;
};

#ifdef BCM_HOST
#else
#endif

class LinuxPlatform : public PlatformBase
{
public:
    LinuxPlatform() : PlatformBase("") {}

    static void Init();
    float GetTimeSeconds() const override;
    void Delay(unsigned int) const override;
    bool PollEvent() override;
};

typedef LinuxPlatform Platform;

extern Platform GPlatform;
