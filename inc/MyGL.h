// MyGL.h

#include <iostream>

/* BCM_HOST: If running on Raspberry Pi, use vendor-specific implementation of
 * EGL to obtain a fullscreen hardware-accelerated OpenGL ES2 context.
 * 
 * Otherwise, on development platform use OpenGL ES2 context provided by GLFW
 * to run the badge program in a desktop window.
 */

#ifdef BCM_HOST
#include "bcm_host.h"
#include "GLES2/gl2.h"
#include "EGL/egl.h"
#include "EGL/eglext.h"
#else
#define GLFW_INCLUDE_ES2
#include <GLFW/glfw3.h>
#endif

#define GL_ASSERT {GLenum err = glGetError(); if(err != GL_NO_ERROR){std::cout<<"GL error: " << err << " Function: " << __FUNCTION__ << "; " << __LINE__ <<std::endl; }}
