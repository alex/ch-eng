#pragma once

#include <string>

class BadgeProgram
{
public:
    virtual ~BadgeProgram() {}
    virtual void HandleClick(float x, float y) {}
    virtual void HandleKeyPress(unsigned char key) {}
    virtual bool Render(float delta) = 0;
};

class MainMenuProgram : public BadgeProgram
{
public:
    bool Render(float delta) { return false; }
};
