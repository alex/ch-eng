#pragma once

// ortho size not screen size, using device vertically
#define SIZE_X ((int)(800))
#define SIZE_Y ((int)(480))
#define APP_SIZE_Y SIZE_X
#define APP_SIZE_X SIZE_Y
